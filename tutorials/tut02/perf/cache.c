#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE    10485760
#define NBR     10000

int main() 
{
    srand(time(NULL));

    // array of data
    int* a = malloc(SIZE*sizeof(int));
    
    for (int i = 0; i < SIZE; i++)
        a[i] = i;
    
    // array of random index
    int* index = malloc(NBR*sizeof(int));
    
    for (int k = 0; k < NBR; ++k)
    //    index[k] = rand() % SIZE;
     	index[k] = k;

    int d = 0;
    
    for (int k = 0; k < 1000; ++k) {
        int c = 0;

        // random fashion access
        for (int n = 0; n < NBR; ++n)
            c += a[index[n]];

        d += c;
    }

    printf("%i\n", d);
    
    free(a);
    free(index);

    return 0;
}
