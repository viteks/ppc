#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// funkce, ktera se zavola pri ukonceni programu pomoci funkce exit
void uklid ()
{
    printf("uklizim!\n");
}

int main()
{
    // po zavolani funkce exit se provede funkce uklid
    atexit(uklid);
    fork();
    exit(0);
}