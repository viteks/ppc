#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
    printf("(%i) #1\n", getpid());
    if (fork() != 0) // pokud je rodic, vytvor potomka
    {
        printf("(%i) #2\n", getpid());
        if (fork() != 0)
            printf("(%i) #3\n", getpid());
    }
    printf("(%i) Konec\n", getpid());
    
    return 0;
}