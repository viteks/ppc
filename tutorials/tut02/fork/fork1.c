#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
    int x = 10;

    if (fork() == 0)
    {
        // potomek
        printf("Potomek (PID: %i), x = %i\n", getpid(), x++);
    }
    else
    {
        printf("Rodic (PID = %i), x = %i\n", getpid(), x--);
    }
    
    // kod spolecny pro rodice i potomka
    printf("Konec (PID: %i), x = %i\n", getpid(), x);

    return 0;
}