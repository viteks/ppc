#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
    printf("(%i) #1\n", getpid());
    fork();
    printf("(%i) #2\n", getpid());
    fork();
    printf("(%i) #3\n", getpid());
    fork();
    printf("(%i) Konec\n", getpid());
    
    return 0;
}