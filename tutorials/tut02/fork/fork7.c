#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

#define N 5
#define KILL 1

int main()
{
    pid_t pid[N];
    int status;

    for (int i = 0; i < N; i++)
    {
        if ((pid[i] = fork()) == 0)
        {
#ifndef KILL
            exit(100+i);
#else
            while(1);
#endif
        }
    }
#ifdef KILL
    for (int i = 0; i < N; i++)
    {
        printf("Kill %i\n", pid[i]);
        kill(pid[i], SIGINT);
    }
#endif

#ifdef WAIT
    for (int i = 0; i < N; i++)
    {
        pid_t p = wait(&status);
        printf("Potomek %i skoncil, status je %i\n", p, WEXITSTATUS(status));
    }
#else
    for (int i = N-1; i >= 0; i--)
    {
        pid_t p = waitpid(pid[i], &status, 0);
        printf("Potomek %i skoncil, status je %i\n", p, WEXITSTATUS(status));
    }
#endif

    return 0;
}
