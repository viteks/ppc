#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void uklid()
{
    printf("Uklizim po PID = %i\n", getpid());
}

int main()
{
    atexit(uklid);

    if (fork() == 0)
    {
        // potomek, ktery hned i skonci
        printf("Potomek, PID = %i\n", getpid());
        exit(0);
    }
    else
    {
        // rodic, ktery se zacykli a neskonci (zombie)
        printf("Rodic, PID = %i\n", getpid());
        while(1);
    }
    return 0;
}