#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>

#define N 10000
#define M 2
#define MAX 100
#define RAND 1

int main()
{
#ifdef RAND
    srand(time(NULL));
#endif

    int fd[2];
    if (pipe(fd) == -1)
    {
        return 1;
    }

    int *a = malloc(N*sizeof(int));

    for (int i = 0; i < N; i++)
#ifdef RAND
        a[i] = rand() % MAX;
#else
        a[i] = i % MAX;
#endif

    for (int i = 0; i < M; i++)
    {
        if (fork() == 0)
        {
            unsigned int sum = 0;
            
            for (int j = i*(N/M); j < (i+1)*(N/M); j++)
            {
                sum += a[j];
            }
            printf("child writes %i\n", sum);
            write(fd[1], &sum, sizeof(sum));
            exit(0);
        }
    }

    int total_sum = 0;

    for (int i = 0; i < M; i++)
    {
        int sum_child;
        read(fd[0], &sum_child, sizeof(sum_child));
        printf("parent reads %i\n", sum_child);
        total_sum += sum_child;
        wait(NULL);
    }

    printf("total sum: %i\n", total_sum);

    close(fd[0]);
    close(fd[1]);
    free(a);

    return 0;
}