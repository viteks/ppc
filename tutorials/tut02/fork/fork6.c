#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
    int status;

    if (fork() == 0)
    {
        printf("Potomek %i chce skoncit\n", getpid());
        exit(0);
    }
    else
    {
        printf("Cekam na potomka\n");
        wait(&status);
        printf("Potomek skoncil\n");
    }
    printf("Konci: %i\n", getpid());
    return 0;
}