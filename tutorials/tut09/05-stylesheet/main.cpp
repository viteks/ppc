#include <QApplication>
#include <QPushButton>
#include <QHBoxLayout>
#include <QFile>

int main(int argc, char * argv[])
{
    QApplication app(argc, argv);
    Q_INIT_RESOURCE(style);

    QFile styleSheet(":/files/style.qss");

    if (!styleSheet.open(QIODevice::ReadOnly)) {
        qWarning("Unable to open :/files/style.qss");
    }

    app.setStyleSheet(styleSheet.readAll());
#ifdef SIMPLE
    QPushButton b ("ahoj");
    b.show();
#else
    QWidget w;
    QHBoxLayout l;
    // b1
    QPushButton b1;
    b1.setObjectName("minus");
    // b2
    QPushButton b2;
    // b3
    QPushButton b3;
    b3.setObjectName("plus");
    // layout
    l.addWidget(&b1);
    l.addWidget(&b2);
    l.addWidget(&b3);
    w.setLayout(&l);
    w.show();
#endif
    return app.exec();
}
