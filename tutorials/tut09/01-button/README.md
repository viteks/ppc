## Kompilace

Připravit předpis pro automatické sestavení programu je možné hned několika způsoby

### qmake

```shell
$ qmake
$ make
```

Vyčištění adresáře

```shell
$ make clean
```

Vyčištění adresáře včetně všech vygenerovaných souborů

```shell
$ make distclean
```

### CMake + make

Stránka o použití CMake a Qt: https://doc.qt.io/qt-6/cmake-get-started.html

```shell
$ mkdir build
$ cd build
$ cmake ../ -G "Unix Makefiles" -DCMAKE_CXX_COMPILER=g++
$ make
```

Vyčištění adresáře

```shell
$ make clean
```

Vyčištění adresáře včetně všech vygenerovaných souborů (nejjednodušší cesta, pozor na pracovní adresář)

```shell
$ rm -rf *
```

### CMake + ninja

```shell
$ cd build
$ cmake ../ -G Ninja -DCMAKE_CXX_COMPILER=g++
$ ninja
```

Vyčištění adresáře

```shell
$ ninja clean
```

## Deployment

Pokud je projekt ve OS Windows překládán mimo QtCreator (nebo pokud nejsou Qt dll knihovny v cestě operačního systému), je třeba dodat dll knihovny, které aplikace potřebuje pro svůj běh. Následující příkaz předpokládá, že pracovní adresář obsahuje spustitelný (exe) soubor.

```shell
$ windeployqt 01-button.exe
```
