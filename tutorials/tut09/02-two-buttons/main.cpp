#include <QApplication>
#include <QPushButton>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    
    QWidget w;
    
    w.resize(100, 100);
    
    QPushButton b1("Button 1", &w);
    QPushButton b2("Button 2", &w);
    b2.move(0, 30);
    
    w.show();
    
    return app.exec();
}
