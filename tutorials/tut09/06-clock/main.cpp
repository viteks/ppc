#include <QApplication>
#include <QPushButton>
#include <QFile>
#include <QtMath>

int main(int argc, char * argv[])
{
    QApplication app(argc, argv);
    Q_INIT_RESOURCE(style);

    QFile styleSheet(":/files/style.qss");

    if (!styleSheet.open(QIODevice::ReadOnly)) {
        qWarning("Unable to open :/files/style.qss");
    }

    app.setStyleSheet(styleSheet.readAll());

    QWidget w;
    w.resize(400, 400);

    int cx = 170, cy = 170, r = 160, x, y;

    for (int i = 1; i < 13; i++)
    {   
        QString name = QString("%1").arg((i+2)%12+1);
        QPushButton *b  = new QPushButton(name, &w);
        x = cx + int(r*qCos((2*M_PI/12)*i));
        y = cy + int(r*qSin((2*M_PI/12)*i));
        b->move (x, y);
        b->setProperty("odd", (i%2) ? true : false);
    }
    
    w.show();

    return app.exec();
}
