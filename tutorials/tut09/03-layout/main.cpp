#include <QApplication>
#include <QHBoxLayout>
#include <QPushButton>

int main (int argc, char ** argv)
{
        QApplication app (argc, argv);
        
        QWidget w;
        QHBoxLayout l;
        
        for (int i = 0; i < 10; i++) 
        {
                QString name = QString("%1 %2").arg("button").arg(i);
                l.addWidget (new QPushButton (name));
        }

        w.setLayout (&l);
        w.show();

        return app.exec();
}
