#include <QApplication>
#include <QPushButton>
#include <QHBoxLayout>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QWidget w; 

    QHBoxLayout l;

    for (int i = 0; i < 10; i++)
    {
        QString name  = QString("%1").arg(i);
        QPushButton *b = new QPushButton(name); 
        if (i%2)
            b->setStyleSheet("background-color: blue; font: bold 14px; width: 40px; height: 40px; border-width: 2px; border-radius: 20px;");
        else
            b->setStyleSheet("background-color: red; font: bold 14px; width: 40px; height: 40px; border-width: 2px; border-radius: 20px;");
        l.addWidget(b);
    }
    
    w.setLayout(&l);

    w.show(); 

    return app.exec();
}
