#include <iostream>
#include <thread>
#include <chrono>
#include <mutex>

using namespace std::chrono_literals;

std::mutex m;

void funkce (int id, int n)
{
    m.lock();
    for (int i = 0; i < n; i++)
    {
        std::cout << "[" << id << "] bude 100ms spat\n";
        std::this_thread::sleep_for(100ms); 
    }
    m.unlock();
}

int main()
{
    std::thread t1(funkce, 1, 2);
    std::thread t2(funkce, 2, 4);
    t1.join();
    t2.join();

    return 0;
}