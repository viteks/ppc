#include <iostream>
#include <thread>
#include <mutex>

std::mutex m;

void funkce1 ()
{
    std::cout << "vlakno t1\n";
}

void funkce2 (int i)
{
    m.lock();
    std::cout << "vlakno i = " << i << "\n";
    m.unlock();
}

void funkce3 (int * i)
{
    *i = 10;
}

void funkce4 (int & i)
{
    i = 20;
}

int main()
{
    int a = 0, b = 0;
    std::thread t1(funkce1);
    std::thread t2(funkce2, 10);
    std::thread t3(funkce2, 20);
    std::thread t4(funkce2, 30);
    std::thread t5(funkce3, &a);
    std::thread t6(funkce4, std::ref(b));
    // pockam na ukonceni vsech vlaken
    t1.join();
    t2.join();
    t3.join();
    t4.join();
    t5.join();
    t6.join();

    std::cout << "a = " << a << std::endl;
    std::cout << "b = " << b << std::endl;

    return 0;
}