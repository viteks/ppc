#include <iostream>

template<typename T>
class Pole
{
    T * pole;
public:
    Pole (int velikost = 10)
    {
        pole = new T[velikost];
    }
    ~Pole ()
    {
        delete [] pole;
    }
    T & operator[] (int idx) {return pole[idx];}
};

int main()
{
    Pole<char> a;

    for (int i = 0; i < 10; i++)
    {
        a[i] = i + 70;
        std::cout << a[i] << std::endl;
    }

    return 0;
}