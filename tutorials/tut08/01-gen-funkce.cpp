#include <iostream>
#include <vector>

/*
    std::vector<int> a;
*/

template<typename T>
std::ostream & operator<< (std::ostream & os, std::vector<T> v)
{
    os << "[ ";
    for (auto i : v) os << i << " ";
    return os << "]\n";
}

template<typename T> 
void swap (T & a, T & b)
{
    int tmp = a;
    a = b;
    b = tmp;
}

template<typename A>
A max (A a, A b)
{
    return a > b ? a : b;
}

template<typename A, typename B>
A min (A a, B b)
{
    // 
}

int main()
{

    int a = 10, b = 20;
    char c = 'b', d = 'a';

    std::cout << "a = " << a << ", b = " << b << "\n";
    swap (a, b);
    std::cout << "a = " << a << ", b = " << b << "\n";
    swap<int> (a, b);
    std::cout << "a = " << a << ", b = " << b << "\n";

    std::cout << ::max(10, 20) << std::endl;
    std::cout << ::max(c, d) << std::endl;
    std::cout << ::max<int>(c, d) << std::endl;

    std::vector<int> e = {10, 20, 30};
    std::cout << e;

    std::vector<char> f = {'a', 'h', 'o', 'j'};
    std::cout << f;

    return 0;
}


