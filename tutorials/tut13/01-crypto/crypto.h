#include <QEventLoop>
#include <QWidget>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QPushButton>
#include <QTimer>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

#include "qcustomplot.h"

class CryptoWidget : public QWidget
{
    Q_OBJECT

    QTimer *t;

    QCustomPlot *plot;
    QVector<double> x, y;

    unsigned long T;

public:
    CryptoWidget (QWidget * parent = 0) : QWidget(parent)
    {
        T = 0;
        // nastaveni databaze
        QSqlDatabase db =  QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName ("crypto.db");
        if (!db.open())
        {
            qDebug() << db.lastError().text();
        }
        // vytvoreni databaze, pokud jeste neexistuje
        QSqlQuery query;
        query.exec("CREATE TABLE IF NOT EXISTS prices (id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR (20), value REAL, timestamp INTEGER);");
        // ziskani dat, ktera uz v databazi jsou
        query.exec("SELECT id, value FROM prices");
        while (query.next())
        {
            x.push_back (query.value(0).toDouble());
            y.push_back (query.value(1).toDouble());
        }

        plot = new QCustomPlot(this);
        plot->resize(640, 480);

        plot->addGraph();
        plot->graph(0)->setPen(QPen(Qt::blue));
        plot->graph(0)->setData(x, y);
		plot->graph(0)->rescaleAxes();
        plot->replot();

        t = new QTimer();
        t->start(10000);
        connect (t, QTimer::timeout, this, CryptoWidget::fetch);
    } 

public slots:
    void fetch ()
    {
        QEventLoop loop;
        QNetworkAccessManager mgr;
        // signal mezi odpovedi a ukoncenim event loop
        QObject::connect (&mgr, QNetworkAccessManager::finished, &loop, QEventLoop::quit);
        QNetworkRequest req (QUrl ("https://api.cryptonator.com/api/ticker/doge-usd"));
        QNetworkReply *rep = mgr.get(req);
        loop.exec();

        if (rep->error() == QNetworkReply::NoError)
        {
            QString rep_str = (QString)rep->readAll();
            QJsonDocument res_json = QJsonDocument::fromJson(rep_str.toUtf8());
            QJsonObject data = res_json.object();
            unsigned long timestamp = data["timestamp"].toDouble();
            if (T == timestamp) return;
            T = timestamp;
            QJsonObject ticker = data["ticker"].toObject();
            double value = QString(ticker["price"].toString()).toDouble();
            QString base = ticker["base"].toString();
            QString sql = QString("INSERT INTO prices (name, value, timestamp) VALUES ('%1', %2, %3)").arg(base).arg(value).arg(timestamp);
            qDebug() << sql;
            QSqlQuery query (sql);
            query.exec();
            x.push_back(query.lastInsertId().toDouble());
            y.push_back(value);
            plot->graph(0)->setData(x, y);
            plot->graph(0)->rescaleAxes();
            plot->replot();
        }
    }
};