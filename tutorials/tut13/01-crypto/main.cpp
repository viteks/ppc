#include <QApplication>

#include "crypto.h"

int main(int argc, char ** argv)
{
    QApplication a (argc, argv);

    CryptoWidget w;
    w.show();

    return a.exec();
}
