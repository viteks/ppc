#include <QEventLoop>
#include <QWidget>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QTimer>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

#include "qcustomplot.h"

class CryptoWidget : public QWidget
{
    Q_OBJECT

    QTimer *t;

    QMap<QString, unsigned long> T;
    QVector<QString> coins;
    QMap<QString, QVector<double>> x, y;
    QCustomPlot *plot;

public:
    CryptoWidget (QWidget * parent = 0) : QWidget(parent)
    {
        coins.push_back ("KSM");
        coins.push_back ("LTC");
        
        // casovac pro automaticke spousteni funkce collect
        t = new QTimer;
        t->start(3000);
        connect (t, QTimer::timeout, this, CryptoWidget::collect);
        // pripojeni k databazi
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName("crypto.db");
        if(!db.open())
        {
            qDebug() << db.lastError().text(); 
        }
        // vytvorim tabulku na ukladani dat
        QSqlQuery query;
        query.exec("CREATE TABLE IF NOT EXISTS prices (id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(20), price REAL, timestamp INTEGER)");
        // jaka data mam jiz v databazi?
        query.exec("SELECT id, price, name FROM prices");
        while (query.next())
        {
            // id
            x[query.value(2).toString()].push_back(query.value(0).toDouble());
            // price
            y[query.value(2).toString()].push_back(query.value(1).toDouble());
        }

        // vytvoreni a konfigurace grafu
        plot = new QCustomPlot(this);
        plot->yAxis->setLabel("price (USD)");
        plot->yAxis2->setVisible(true);
        plot->legend->setVisible(true);
        plot->legend->setBrush(QColor(255, 255, 255, 150));
        plot->resize(640, 480);
        int gi = 0;
        for (auto coin : coins)
        {
            T[coin] = 0;
            QColor color(20+200/4.0*gi,70*(1.6-gi/4.0), 150, 150);
            if (gi == 1)
                plot->addGraph(plot->xAxis2, plot->yAxis2);
            else
                plot->addGraph();
            plot->graph()->setLineStyle(QCPGraph::lsLine);
            plot->graph()->setPen(QPen(color.darker(200)));
            plot->graph()->setBrush(color);
            plot->graph()->setName(coin);
            plot->graph()->setData(x[coin], y[coin]);
            plot->graph()->rescaleAxes();
            gi++;
        }
        // vykresleni dat
        plot->replot();
    } 

public slots:

    void collect ()
    {
        for (int i = 0; i < coins.size(); i++)
        {
            fetch(coins[i], i);
        }
    }

    void fetch (QString name, int num)
    {
        QEventLoop loop;
        QNetworkAccessManager mgr;
        QObject::connect (&mgr, QNetworkAccessManager::finished, &loop, QEventLoop::quit);
        QNetworkRequest req (QUrl (QString("https://api.cryptonator.com/api/ticker/%1-usd").arg(name)));
        QNetworkReply *rep = mgr.get(req);
        loop.exec();

        if (rep->error() == QNetworkReply::NoError)
        {
            QString rep_str = (QString)rep->readAll();
            QJsonDocument res_json = QJsonDocument::fromJson(rep_str.toUtf8());
            QJsonObject data = res_json.object();
            // timestamp
            unsigned long timestamp = data["timestamp"].toDouble();
            if (T[name] == timestamp) return;
            T[name] = timestamp;
            qDebug() << timestamp;
            QJsonObject ticker = data["ticker"].toObject();
            // price
            double price = QString(ticker["price"].toString()).toDouble();
            qDebug() << price;
            QString base = ticker["base"].toString();
            qDebug() << base;
            // vlozeni dat do databaze
            QSqlQuery query;
            QString sql = QString("INSERT INTO prices (name, price, timestamp) VALUES ('%1', %2, %3)").arg(base).arg(price).arg(timestamp);
            qDebug() << sql;
            query.exec(sql);

            // pridani dat do vektoru
            x[name].push_back(query.lastInsertId().toDouble());
            y[name].push_back(price);
            plot->graph(num)->setData(x[name], y[name]);
            plot->graph(num)->rescaleAxes();
            plot->replot();
        }
    }
};