#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>
// fib: 0 1 1 2 3 5 8 13 21 34 ...
// N prvku posloupnosti
// 1: 0 1
// 2: 1 1
// 3: 1 2
// 4: 2 3

class fibonacci
{
    int prvni, druha;
public:
    fibonacci () : prvni(0), druha(1) {}
    int operator() ()
    {
        int tmp = prvni;
        prvni = druha;
        druha += tmp;
        return tmp;
    }
};

int main()
{
    std::vector<int> a;

    std::generate_n (std::back_inserter(a), 20, fibonacci());
    std::copy (a.begin(), a.end(), std::ostream_iterator<int> (std::cout, " "));
    std::cout << std::endl;

    return 0;
}