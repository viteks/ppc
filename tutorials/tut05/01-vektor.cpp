#include <iostream>
#include <algorithm>
#include <numeric>

class Iterator
{
    int * ptr;
public:
    Iterator (int * x) : ptr(x) {}
    friend bool operator!= (const Iterator & a, const Iterator & b) {return a.ptr != b.ptr;}
    Iterator operator++ () {ptr++; return *this;}
    int & operator* () const {return *ptr;}
};

class Vektor
{
    int * pole;
    int velikost;
public:
    Vektor () : velikost(0) {pole = new int[1];}
    Vektor (int x) : velikost(x) {pole = new int [x];}
    ~Vektor () {delete [] pole;}
    int size() const {return velikost;}
    void resize (int x)
    {
        // dynamicka alokace pole nove delky
        int * tmp = new int [x];
        // kopie stareho pole do nove alokovaneho
        std::copy (pole, pole+velikost, tmp);
        // dealokace stareho pole
        delete [] pole;
        // aktualizace ukazatele na novy blok pameti
        pole = tmp;
    }
    void push_back (int x)
    {
        resize(velikost+1);
        pole[velikost++] = x;
    }
    int & operator[] (int x) {return pole[x];}
    // funkce vyuzivajici iterator
    Iterator begin() {return Iterator (&pole[0]);}
    Iterator end()   {return Iterator (&pole[velikost]);}
};

std::ostream & operator<< (std::ostream & p, Vektor & v)
{
    p << "[ ";
    for (auto i : v)
        p << i << " ";
    p << "]";

    return p;
}

int mult(int x, int y)
{
    return x * y;
}

int main ()
{
    // vektor nulove delky
    Vektor a;

    for (int i = 0; i < 10; i++)
        a.push_back (i);

    a[0] = 100;

    for (int i = 0; i < a.size(); i++)
        std::cout << a[i] << " ";
    std::cout << std::endl;

    /* 
        for (auto i = a.begin(); i != a.end(); i++)
            std::cout << *i
    */

    for (auto i : a)
        std::cout << i << " ";
    std::cout << std::endl;

    std::cout << a << std::endl;

    std::cout << "suma: " << std::accumulate (a.begin(), a.end(), 0) << std::endl;

    std::cout << "mult: " << std::accumulate (a.begin(), a.end(), 1, mult) << std::endl;

    // [&] () {};
    // (y1 + y2 + .. + yN) / N
    // y1/N + y2/N + .. +yN/N
    auto lambda = [&] (double x, double y) {return x + y/a.size();};
    std::cout << "arit: " << std::accumulate (a.begin(), a.end(), 0.0, lambda) << std::endl;
    // slo by neco podobneho udelat, i kdybychom neznali velikost kontejneru?
    // ANO! :)
    // https://en.wikipedia.org/wiki/Moving_average#Cumulative_moving_average
    auto lambda2 = [N = 0] (double x, double y) mutable {return x + (y-x)/++N;};
    std::cout << "arit: " << std::accumulate (a.begin(), a.end(), 0.0, lambda2) << std::endl;
    // nebo alternativne takto
    int N = 0;
    auto lambda3 = [&N] (double x, double y) {return x + (y-x)/++N;};
    std::cout << "arit: " << std::accumulate (a.begin(), a.end(), 0.0, lambda3) << std::endl;

    return 0;
}
