#include <QCoreApplication>
#include <QHostInfo>
#include <QUdpSocket>
#include <QDateTime>

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	QHostInfo HostInfo = QHostInfo::fromName("tak.cesnet.cz");

	QUdpSocket *UdpSocket= new QUdpSocket();

	UdpSocket->connectToHost(QHostAddress(HostInfo.addresses().at(0)), 123);

	char m[48] = {010, 0, 0, 0, 0, 0, 0, 0, 0};

	UdpSocket->writeDatagram(m, sizeof(m), QHostAddress(HostInfo.addresses().at(0)), 123);

	if (UdpSocket->waitForReadyRead()) {
		while (UdpSocket->hasPendingDatagrams()) {
			QByteArray data = UdpSocket->readAll();
			
			qDebug() << "NTP response: " << data;
			
			if (data.size() == 48) {
		
				unsigned long cas = uchar(data[43]) + (uchar(data[42]) << 8) + (uchar(data[41]) << 16) + (uchar(data[40]) << 24);
				cas -= 2208988800;
				qDebug() << "Secs since epoch: " << cas;
				QDateTime time = QDateTime::fromSecsSinceEpoch(cas);
				qDebug() << "Time: " << time.toString("yyyy-MM-dd HH:mm:ss");
			}
		}
	}

	delete UdpSocket;

	return 0;
}
