#include <QApplication>
#include <QtCore/QUrl>
#include <QDebug>
#include <QSettings>
#include <QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QWidget>

#include "qcustomplot.h"

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	QWidget *w = new QWidget;

	w->resize(640, 480);

	QVector<double> y;
	QVector<double> x;
	int count = 0;

	QCustomPlot *plot = new QCustomPlot(w);
	plot->addGraph();
	plot->graph(0)->setPen(QPen(Qt::blue));
	plot->resize(640, 480);
	
	QEventLoop eventLoop;
	QNetworkAccessManager mgr;
	QObject::connect (&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));
	QNetworkRequest req (QUrl (QString("https://onemocneni-aktualne.mzcr.cz/api/v1/covid-19/nakazeni-vyleceni-umrti-testy.json")));
	QNetworkReply *reply = mgr.get (req);
	eventLoop.exec();

	if (reply->error() == QNetworkReply::NoError) 
	{
		QString strReply = (QString)reply->readAll();
		// qDebug() << strReply;
		QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
		QJsonObject jsonObj = jsonResponse.object();
		QJsonArray jsonArray = jsonObj["data"].toArray();
		// qDebug() << jsonArray;
		for (auto v : jsonArray)
		{
			QJsonObject obj = v.toObject();
			// qDebug() << obj["datum"].toString();
			// qDebug() << obj["kumulovany_pocet_umrti"].toDouble();
			x.push_back(double(count++));
			y.push_back(obj["kumulovany_pocet_umrti"].toDouble());	
		}
		delete reply;
	}
	else 
	{
		qDebug() << "chyba" << reply->errorString();
		delete reply;
	}

	plot->graph(0)->setData(x, y);
	plot->graph(0)->rescaleAxes();
	plot->replot();

	w->show();

	return app.exec();
}
