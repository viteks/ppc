#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QRandomGenerator>
#include <QPainter>
#include <QPainterPath>

class Ball : public QGraphicsItem
{
	qreal angle, speed;

	public:
	Ball ()
	{
		angle = QRandomGenerator::global()->generate() % 360;
		setRotation(angle);
		speed = 2;
		int startX = QRandomGenerator::global()->generate() % 200;
		int startY = QRandomGenerator::global()->generate() % 200;

		setPos (startX, startY);
	}

	QRectF boundingRect () const
	{
		return QRectF (0, 0, 30, 30);
	}

	void paint (QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0)
	{
		Q_UNUSED (option);
		Q_UNUSED (widget);

		QRectF okraj = boundingRect();
		QBrush vypln (Qt::gray);

		if (scene()->collidingItems(this).isEmpty())
		{
			// zadne objekty nekoliduji s timto nekoliduji
		}
		else
		{
			vypln.setColor (Qt::red);
			setRotation (rotation() + 180);
		}

		painter->fillRect(okraj, vypln);
	}

	void advance (int phase)
	{
		if (!phase) return;

		setPos (mapToParent(0, speed));
	}

};

