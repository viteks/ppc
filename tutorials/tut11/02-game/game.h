#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QTimer>

#include "ball.h"

class Game : public QWidget
{
	QGraphicsView * board;
	QGraphicsScene * scene;
	QGraphicsRectItem *c1, *c2;
	QTimer *t;
	Ball *b;
	public:
	Game (QWidget * parent = 0) : QWidget (parent)
	{
		// plocha, do ktere pozdeji vlozime scenu
		board = new QGraphicsView(this);
		board->setRenderHint(QPainter::Antialiasing);
		board->resize(400, 400);

		// scena, do ktere se budou vkladat jednotlive komponenty - gr. objekty/primitiva
		scene = new QGraphicsScene(this);
		scene->setSceneRect(0, 0, 350, 350);
		// vytvoreni okraju
		QPen pero (Qt::red);
		QRectF okraj = scene->sceneRect(); 
		scene->addLine (QLineF (okraj.topLeft(), okraj.topRight()), pero);
		scene->addLine (QLineF (okraj.bottomLeft(), okraj.bottomRight()), pero);
		scene->addLine (QLineF (okraj.topLeft(), okraj.bottomLeft()), pero);
		scene->addLine (QLineF (okraj.topRight(), okraj.bottomRight()), pero);

		// vlozeni hraciho prvku
		c1 = new QGraphicsRectItem (0, 0, 30, 30);
		c1->setBrush (QBrush(Qt::green));
		scene->addItem (c1);

		c2 = new QGraphicsRectItem (0, 0, 30, 30);
		c2->setBrush (QBrush(Qt::blue));
		scene->addItem (c2);
		c2->setPos(10, 200);

		b = new Ball;
		scene->addItem (b);

		c1->setPos(10, 10);
		t = new QTimer(this);
		t->start(10);
		// QObject::connect (t, QTimer::timeout, this, Game::tick);
		QObject::connect (t, QTimer::timeout, scene, QGraphicsScene::advance);

		// vlozim scenu do boardu
		board->setScene(scene);
	}
	public slots:
		void tick ()
		{
			c1->moveBy(0, 10);

			if (c1->collidesWithItem(c2))
			{
				c1->setBrush (Qt::red);
				c1->setRotation(90);
			}
		}
};

