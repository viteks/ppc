#include <QApplication>
#include <QFileDialog>
#include <QColorDialog>
#include <QMessageBox>
#include <QLabel>
#include <QPixmap>
#include <QImage>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>

int main(int argc, char ** argv)
{
	QApplication a(argc, argv);

	QWidget w;

	QHBoxLayout hbox;
	QVBoxLayout vbox;
	QPushButton b1("nahrat obrazek"), b2("vyplnit barvou"), b3("odbarvit");
	QLabel l;

	hbox.addWidget(&b1);
	hbox.addWidget(&b2);
	hbox.addWidget(&b3);

	vbox.addLayout(&hbox);
	vbox.addWidget(&l);

	w.setLayout(&vbox);

	QImage obrazek;
	bool obrazek_nahrany = false;

	QObject::connect (&b1, QPushButton::clicked, [&](){
		QString fileName = QFileDialog::getOpenFileName(&w, "Open Image", 
			QDir::currentPath(), 
			"Image Files (*.png *.jpg *.bmp)");

		obrazek = QImage(fileName);
		l.setPixmap(QPixmap::fromImage(obrazek));
		obrazek_nahrany = true;
	});

	QObject::connect(&b2, QPushButton::clicked, [&](){
		QColor color = QColorDialog::getColor(Qt::yellow, &w);
		obrazek = QImage(582, 582, QImage::Format_RGB32);
		obrazek.fill(color);
		l.setPixmap(QPixmap::fromImage(obrazek));
	});

	QObject::connect(&b3, QPushButton::clicked, [&](){

		if (!obrazek_nahrany)
		{
			QMessageBox msg;
			msg.critical (&w, "Chyba", "Je treba nahrat obrazek!");
			return;
		}

		for (int i = 0; i < obrazek.width(); i++)
		{
			for (int j = 0; j < obrazek.height(); j++)
			{
				QColor pixel = obrazek.pixelColor(i, j);
				int H = pixel.hue();
				int V = pixel.value();
				obrazek.setPixel(i, j, pixel.fromHsv(H, 0, V).rgb());
			}
		}
		l.setPixmap(QPixmap::fromImage(obrazek));
	});

	w.show();

	return a.exec();
}

