#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <sstream>

struct Stranka
{
    std::string text;
    int volby;
    std::vector<int> rozcestnik;
    std::vector<std::string> predmety;
};

class Hra
{
    std::vector<Stranka> scenar;
    std::vector<std::string> predmety;
    char c;
    int pruchody;
    std::ifstream is;
public:
    Hra () : pruchody (0) {}
    void load(const char *filename)
    {
        is = std::ifstream(filename);
        // std::string a = std::string("ahoj");
        while (!is.eof())
        {
            Stranka tmp;
            // nacteni textove informace
            std::getline(is, tmp.text);
            // nacteni predmetu
            std::string kramy, predmet;
            std::getline(is, kramy);
            std::stringstream ss (kramy);
            while (std::getline (ss, predmet, ';'))
            {
                tmp.predmety.push_back(predmet);
            }
            // nacteni poctu voleb
            is >> tmp.volby;
            // vytvoreni rozcestniku
            if (tmp.volby > 0)
                std::copy_n(std::istream_iterator<int>(is), tmp.volby, std::back_inserter(tmp.rozcestnik));
            // odstraneni prebytecnych znaku
            do
            {
                is.get(c);
            } while (c != '\n');
            scenar.push_back(tmp);
        }
    }
    void hraj(int next = 0)
    {
        do
        {
            Stranka tmp = this->scenar[next];
            std::cout << tmp.text << "\n";
            if (tmp.volby > 0)
            {
                std::cout << "-\nDostupne predmety:\n";
                char a = 'a', b = 'A';
                for (auto i : tmp.predmety)
                {
                    std::cout << (a++) << "  " << i << "\n";
                }
                std::cout << "-\nMoje predmety:\n";
                for (auto i : this->predmety)
                {
                    std::cout << (b++) << "  " << i << "\n";
                }
                std::cout << "-\nVolba ve hre:\n";
                std::string volba;
                std::cout << "> ";
                std::cin >> volba;
                try
                {
                    next = tmp.rozcestnik[stoi(volba) - 1];
                }
                catch (...)
                {
                    int klic = int(volba[0] - 'a');
                    std::cout << "vybral jsem " << tmp.predmety[klic] << "\n";
                    this->predmety.push_back(tmp.predmety[klic]);
                }
                std::cout << "=======\n";
            }
            else
                next = 0;
        } while (next > 0 || !(pruchody++));
    }
};

int main()
{
    Hra h;
    h.load("02-data.txt");
    h.hraj();
    return 0;
}
