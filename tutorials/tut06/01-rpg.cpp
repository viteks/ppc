#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <iterator>

struct Strana
{
    std::string text;
    int volby;
    std::vector<int> rozcestnik;
};

struct Hra
{
    int volba;
    char c;
    std::vector<Strana> scenar;
    void nahraj (const char * filename)
    {
        std::ifstream is (filename);
        while (!is.eof()) {
            Strana tmp;
            std::getline (is, tmp.text);
            is >> tmp.volby;
            if (tmp.volby > 0)
                std::copy_n(std::istream_iterator<int>(is), tmp.volby, std::back_inserter(tmp.rozcestnik));
            do  { 
                is.get(c);
            } while (c != '\n');
            this->scenar.push_back(tmp);
        }
    }
    void hraj (int next = 0)
    {
        do {
            Strana tmp = this->scenar[next];
            std::cout << tmp.text << "\n";
            if (tmp.volby > 0)
            {
                std::cout << "> ";
                std::cin >> volba;
                next = tmp.rozcestnik[volba-1];
            }
            else next = 0;
        } while (next > 0);
    }
};

int main()
{
    Hra h;
    h.nahraj ("01-data.txt");
    h.hraj();    

    return 0;
}
