#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    double pi = 3.14159265;
    double pp = 65.876866;
    double px = 768748.898; 

    cout << pi << endl << pp << endl << px << endl; 

    cout << setprecision(2);

    cout << pi << endl << pp << endl << px << endl;

    cout << fixed;

    cout << pi << endl << pp << endl << px << endl;

    cout << setprecision(4);

    cout << pi << endl << pp << endl << px << endl;

    cout << scientific;

    cout << pi << endl << pp << endl << px << endl;

    cout.unsetf (ios_base::floatfield); // vratim zpet puvodni smysl vypisu

    cout << pi << endl << pp << endl << px << endl;

    cout << left << setw(15) << pi << pp << px << endl;

    return 0;
}