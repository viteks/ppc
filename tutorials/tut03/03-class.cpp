#include <iostream>

// deklarace tridy -- popis (predpis) pro vytvoreni objektu
// Auto je trida dopravnich prostredku
class Auto
{
    // atributy implicitne private
    int barva;
    int obsah;
    int dvere;
// pokud jsou atributy neverejne, vytvarim v pripade potreby metody (set/get)
// pro rizeny pristup, metody budou verejne
public:
    void nastav_obsah (int muj_obsah) 
    {
        // inicializace atributu objektu
        obsah = muj_obsah;
    }
    // pouze deklarace, definice je mimo deklaci tridy
    int zjisti_obsah ();
    void nastav_dvere (int in_dvere) 
    {
        this->dvere = in_dvere;
    }
    // pouze deklarace, definice je mimo deklaci tridy
    int zjisti_dvere ();
    // jednoduchy konstruktor
    Auto () 
    {
        obsah = 1000;
        std::cout << "zavolan kontruktor Auto\n";
    }
    // pretizeny konstruktor
    Auto (int in_dvere, int in_obsah) 
    {
        obsah = in_obsah;
        dvere = in_dvere;
        std::cout << "zavolan lepsi konstruktor\n";
    }
    // destruktor spolecny vsem instancim
    ~Auto () 
    {
        std::cout << "zavolan destruktor ~Auto\n";
    }
};

// definice metod, ktere byly dosud jen deklarovany
// mimo deklaraci tridy je treba kvalifikovat pomoci ::
int Auto::zjisti_obsah()
{
    return obsah;
}

int Auto::zjisti_dvere()
{
    return dvere;
}

int main()
{
    // trida je popis datoveho typu
    // typ identifikator
    // primitivni datovy typ: int a;
    Auto a, b, c(3, 1200);
    Auto *d = new Auto;
    Auto *e = new Auto(5, 2000);
    // nemam primy pristup k atributu, je treba volat funkci
    // a.obsah = 1600;
    a.nastav_obsah(1600);
    a.nastav_dvere(4);
    // nemam pristup k atributu, je treba volat funkci
    // std::cout << "moje auto ma obsah motoru " << a.obsah << " cm^3\n";
    std::cout << "moje auto (a) ma obsah motoru " << a.zjisti_obsah() << " cm^3\n";
    std::cout << "moje auto (a) ma " << a.zjisti_dvere() << " dvere\n";

    std::cout << "moje auto (b) ma obsah motoru " << b.zjisti_obsah() << " cm^3\n";

    std::cout << "moje auto (c) ma obsah motoru " << c.zjisti_obsah() << " cm^3\n";

    delete d;
    delete e;

    return 0;
}