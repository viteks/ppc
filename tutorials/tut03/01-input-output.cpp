#include <stdio.h>

#include <iostream>

namespace tut03
{
    int a = 100;
    int b = 200;
}

using namespace std;
using namespace tut03;

int main()
{
    printf("Ahoj C++\n");

    std::cout << "Ahoj C++" << std::endl;

    std::cout << "a = " << 10 << "\n";

    int a = 10, b = 30; // ::a, ::b

    std::cout << "a + b = " << (a + b) << "\n";

    cout << "tut03::a + tut03::b = " << (tut03::a + tut03::b) << "\n";

    /*
        std::cout   std. vystup (bufferovany)
        std::cerr   std. chybovy vystup (nebufferovany)
        std::cin    std. vstup
    */

    std::cout << "zadej cislo typu int: ";

    std::cin >> a;

    if (std::cin.good())
        std::cout << "zadal jsi: " << a << std::endl;
    else
        std::cout << "douc se datovy typy! :-)" << std::endl;

    return 0;
}