/*
    prace s kontejnerem std::map
*/

#include <iostream>
#include <iomanip>
#include <map>

int main()
{
    std::map<std::string, int> M;
    // pokud zaznam dosud neexistoval, vlozi se novy
    M["Praha"] = 1000000;
    M["Brno"] = 380000;
    M["Budejovice"] = 100000;
    // existujici zaznam se zmeni
    M["Praha"] = 1200000;
    // alternativne lze vlozit pomoci metody insert
    M.insert({"Plzen", 250000});
    // co se stane, kdyz polozka jiz existuje?
    M.insert({"Praha", 1000000});

    std::string x(21, '-');
    std::cout << x << "\n";
    for (auto m : M)
        std::cout << std::left << std::setw(12) << m.first << "|" << std::setw(8) << std::right << m.second << std::endl;
    std::cout << x << "\n";

    // nalezeni polozky v kontejneru metodou find
    // navratovou hodnotou je iterator
    auto i = M.find("Plzen");
    std::cout << "Plzen: " << i->second << "\n";
    // v tomto pripade nema zadnou extra vyhodu
    std::cout << "Plzen: " << M["Plzen"] << "\n";

    auto ii = M.find("Liberec");
    // find vraci iterator, ktery ukazuje na prvek, 
    // pokud prvek neni nalezen, ma hodnotu end() -- prvni prvek za poslednim prvkem M
    if (ii == M.end()) {
        std::cout << "zaznam nenalezen\n";
    }
    else {
        std::cout << "Liberec: " << ii->second << "\n";
    }
    std::cout << "Liberec: " << M["Liberec"] << "\n";

    return 0;
}