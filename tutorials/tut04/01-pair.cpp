#include <iostream>
#include <utility>
#include <tuple>

int main()
{
    std::pair<int, int> par4, par2(1, 3);
    std::pair<std::string, int> par3 = {"ahoj", 3};

    par4 = std::make_pair(10, 20);

    // pristup k polozkam paru (rozbaleni)
    std::cout << par2.first << " " << par2.second << std::endl;
    // alternativne pomoci std::get
    std::cout << std::get<0>(par2) << " " << std::get<1>(par2) << std::endl;

    int a, b;
    std::tie(a, b) = par2;

    std::cout << a << " " << b << std::endl;

    std::cout << std::get<0>(par3) << " " << std::get<1>(par3) << std::endl;

    // kopirujici konstruktor
    std::pair<int, int> par1(par2);
    std::cout << std::get<0>(par1) << " " << std::get<1>(par1) << std::endl;

    // par1.first = 100;
    // prohozeni objektu
    par2.swap(par1);

    std::cout << std::get<0>(par2) << " " << std::get<1>(par2) << std::endl;

    if (par1 == par2)
    {
        std::cout << "jsou stejne\n";
    }
    else
    {
        std::cout << "nejsou stejne\n";
    }

    return 0;
}