/*
    multimap - asociativni pole s duplicitnim klicem
*/
#include <iostream>
#include <map>
#include <cstdlib>
#include <ctime>

int main()
{
    // inicializace nahodneho generatoru
    std::srand(std::time(nullptr));

    std::multimap <std::string, int> T;

    for (int i = 0; i < 5; i++)
        T.insert({"obyvak", std::rand() % 25});

    for (int i = 0; i < 5; i++)
        T.insert({"kuchyne", std::rand() % 22});

    for (auto t : T)
        std::cout << t.first << " - " << t.second << "\n";

    // jak filtrovat hodnoty? bud find, ktera najde prvni vyskyt
    auto i = T.find("obyvak");
    std::cout << "find: obyvak\n";
    while (i != T.end())
    {
        std::cout << i->first << " - " << i->second << std::endl;
        i++;
    }
    // equal_range vraci par iteratoru
    auto p = T.equal_range("kuchyne");
    // std::multimap<std::string, int>::iterator it = p.first;
    auto it = p.first;
    std::cout << "find: kuchyn\n";
    for (; it != p.second; it++)
        std::cout << it->first << " - " << it->second << std::endl;

    return 0;
}