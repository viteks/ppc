#include <tuple>
#include <iostream>

typedef std::tuple<int, int, int> ttype;

int main()
{
    ttype t1 {10, 20, 30};

    std::cout << std::get<2>(t1) << std::endl;

    std::get<2>(t1) = 10;

    std::cout << std::get<2>(t1) << std::endl;

    std::cout << "pocet prvku: " << std::tuple_size<ttype>::value << std::endl;

    return 0;
}