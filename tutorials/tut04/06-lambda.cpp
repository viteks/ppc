/*
    trideni vektoru vs. lambda funkce
*/

#include <iostream>
#include <vector>
#include <algorithm>

int main ()
{
    // lambda funkce [] () {}
    int y = 20; 
    auto f = [&] (int x) {return ++y*x;};
    std::cout << f(3) << std::endl;

    std::vector<int> A = {10, 30, 22, 11, 9, 5, 3, 0, 4};

    for (int a : A)
        std::cout << a << " ";
    std::cout << std::endl;

    std::sort(A.begin(), A.begin()+5);

    for (int a : A)
        std::cout << a << " ";
    std::cout << std::endl;

    /* 
        algoritmus std::sort predava lambda funkci hodnoty dvou po 
        sobe jdoucich prvku zdroje dat - kontejneru aa
    */
    std::sort(A.begin(), A.end(), [](int a, int b){return a > b;});

    for (int a : A)
        std::cout << a << " ";
    std::cout << std::endl;

    std::vector<int> B;
    B.resize(A.size());

    std::copy (A.begin(), A.end(), B.begin());

    for (int b : B)
        std::cout << b << " ";
    std::cout << std::endl;

    /*
        algoritmus std::copy_if predava lambda funkci hodnotu prvku
        zdroje dat - kontejneru A; pokud je navratova hodnota lambdy
        true, dojde ke zkopirovani do ciloveho kontejneru
    */
    std::vector<int> C;
    C.resize(A.size());

    std::copy_if (A.begin(), A.end(), C.begin(), [](int a){return a > 10;});

    for (int c : C)
        std::cout << c << " ";
    std::cout << std::endl;

    return 0;
}