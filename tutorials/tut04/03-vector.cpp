#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>

int main()
{
    // prazdny (neinicializovany) vektor nulove velikosti
    std::vector<int> a;
    // velikost vektoru?
    std::cout << "velikost vektoru a: " << a.size() << "\n";
    // vektor velikosti 10
    std::vector<int> b(10);
    // velikost vektoru?
    std::cout << "velikost vektoru a: " << b.size() << "\n";
    // inicializace vektoru vyctem prvku
    std::vector<char> c {'z', 'a', 'h', 'o', 'j'};

    // size
    std::cout << "velikost vektoru c: " << c.size() << "\n";
    // kapacita
    std::cout << "kapacita vektoru c: " << c.capacity() << "\n";
    // rezervace mista
    c.reserve(100);
    std::cout << "velikost vektoru c: " << c.size() << "\n";
    std::cout << "kapacita vektoru c: " << c.capacity() << "\n";


    // postupne vkladani prvku na konec vektoru, vcetne automaticke realokace
    for (int i = 0; i < 10; i++)
    {
        a.push_back(i);
    }
    std::cout << "velikost vektoru a: " << a.size() << "\n";

    for (int i = 0; i < 20; i++)
    {
        try {
            // pristup operatorem [] -- nekontrolovany pristup
            std::cout << a[i] << " ";
            // alternativne metodou .at() -- system zachytavani vyjimek
            // std::cout << a.at(i) << " ";
        }
        catch (std::out_of_range & e)
        {
            std::cout << "mas tam problem: " << e.what();
            break;
        }
    }
    std::cout << "\n program pokracuje" << std::endl;

    // iterator -- ukazatel na prvek / prvky kontejneru
    std::vector<char>::iterator  i1;
    // inicializace iteratoru i1 na zacatek kontejneru c
    i1 = c.begin();

    std::cout << *i1++ << "\n";
    std::cout << *i1++ << "\n";

    for (i1 = c.begin(); i1 != c.end(); i1++)
    {
        std::cout << *i1 << " ";
    }
    std::cout << std::endl;

    typedef std::vector<char>::const_iterator cit;

    // range-for (c++11)
    for (auto & p : a)
        p = 10;
    for (auto p : a)
        std::cout << p << " ";
    std::cout << std::endl;

    std::sort (c.begin() + 1, c.end());
    std::copy (c.begin(), c.end(), std::ostream_iterator<char> (std::cout, " | "));
}