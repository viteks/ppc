/*
    v minulem dile jsme videli:

    - jedna trida Auto
    - atribut typu std::string, ktery rozlisuje znacku

    co bychom ve skutecnosti chteli

    - trida Auto definuje rozhrani
    - jednotlive typy jsou popsany svymi vlastnimi tridami
*/
#include <iostream>
#include <string.h>
#include <vector>

namespace Color {
    enum Code {
        FG_RED      = 31,
        FG_GREEN    = 32,
        FG_YELLOW   = 33,
        FG_BLUE     = 34,
        FG_DEFAULT  = 39,
        BG_RED      = 41,
        BG_GREEN    = 42,
        BG_BLUE     = 44,
        BG_DEFAULT  = 49
    };
    class Modifier {
        Code code;
    public:
        Modifier (Code in_code) : code(in_code) {}
        friend std::ostream &
        operator<<(std::ostream& os, const Modifier& mod) {
            return os << "\033[" << mod.code << "m";
        }
    };
}

Color::Modifier red (Color::FG_RED);
Color::Modifier green (Color::FG_GREEN);
Color::Modifier yellow (Color::FG_YELLOW);
Color::Modifier def (Color::FG_DEFAULT);

class Auto
{
// pokud bychom chteli docilit toho, aby nebylo mozne instancionovat 
// tridu auto, musi obsahovat alespon jednu plne virtualni metodu
// virtual void metoda () = 0;
protected:
    std::string znacka, typ;
    int kola, motor;
public:
    Auto (int k) : kola(k) {}
    virtual void info() {std::cout << znacka << " " << typ << " pocet kol: " << kola << "\n";}
};

class Skoda : public Auto
{
public:
    Skoda (std::string t) : Auto (4)
    {
        typ = t;
        znacka = "Skoda";
    }
    void info()
    {
        std::cout << red << "::: Nabidka vozu znacky Skoda :::\n" << def;
        std::cout << "Uzasna " << typ << "\n";
        std::cout << green << std::string(33, '-') << def << "\n";
    }
};

class Tatra : public Auto
{
public:
    Tatra (std::string t, int k = 4) : Auto(k)
    {
        typ = t;
        znacka = "Tatra"; 
    }
};

int main()
{
/*
    Skoda s1 ("Octavia");
    s1.info();
    // std::vector<Skoda *>

    Tatra t1 ("815");
    t1.info();
    // std::vector<Tatra *>

    Auto *a1 = new Skoda("Favorit");
    a1->info ();  
    Auto *a2 = new Tatra("613");
    a2->info ();
*/

    std::vector<Auto *> bazar;
    bazar.push_back(new Skoda("Superb"));
    bazar.push_back(new Tatra("815", 6));
    bazar.push_back(new Tatra("613"));
    bazar.push_back(new Skoda("Octavia"));

    for (auto i : bazar)
    {
        i->info();
    }

    return 0;
}