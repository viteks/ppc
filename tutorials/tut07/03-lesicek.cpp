#include <iostream>
#include <string.h>
#include <typeinfo>

class Zviratko
{
protected:
    std::string druh;
    bool stav;
    int energie;
public:
    Zviratko (std::string d, int e) : druh (d), stav(true), energie(e) {}
    friend std::ostream & operator<< (std::ostream & os, Zviratko & z);
    void operator>> (Zviratko & z) {this->konzumace(z);} 
    operator bool() {return stav;}
    bool operator> (Zviratko &z) {return (this->energie > z.energie);}
    Zviratko & operator+= (Zviratko &z) {this->energie += z.energie; z.stav = false; return *this;} 
    virtual void konzumace (Zviratko & z)
    {
        if (!(*this)) {
            std::cout << *this << " je mrtva!\n";
            return;
        }

        std::cout << *this << " se pokousi sezrat " << z << "\n";

        if (std::addressof(*this) == std::addressof(z))
        {
            std::cout << z << " nemuze sezrat sebe samu!\n";
            return;
        }

        if (typeid(*this) == typeid(z))
        {
            std::cout << "kanibalismus mezi " << z << " neni povolen!\n";
            return;
        }
        
        if (*this > z)
        {
            std::cout << *this << " sezrala " << z;
            *this += z;
        }
    }
};

std::ostream & operator<< (std::ostream & os, Zviratko & z)
{
    return os << z.druh;
}

class Mys;

class Kocka : public Zviratko
{
public:
    Kocka(int e = 1000) : Zviratko("kocka", e) {}
};

class Mys : public Zviratko
{
public:
    Mys(int e = 1000) : Zviratko("mys", e) {}
};

int main()
{
    Kocka k1, k2;
    // std::cout << k1 << std::endl;

    Mys m1;
    // k1.konzumace(m1);
    // k1.konzumace(k2);
    // m1.konzumace(k2);
    // k1.konzumace(m1);
    // m1.konzumace(k2);
    k1 >> m1;
}
