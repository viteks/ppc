#include <iostream>


class Zarizeni
{
protected:
    virtual void init()
    {
        std::cout << "inicializace Zarizeni\n";
    }
public:
    void run ()
    {
        init ();
        // a nejaka dalsi funkcionalita, ktera
        // primo nesouvisi s inicializa
    }
};

class LepsiZarizeni : public Zarizeni
{
    void init()
    {
        Zarizeni::init();
        std::cout << "inicializace LepsiZarizeni\n";
    }

};

int main()
{
    Zarizeni a;
    a.run();

    LepsiZarizeni b;
    b.run();

    return 0;
}