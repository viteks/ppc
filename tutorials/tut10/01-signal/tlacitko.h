#include <QPushButton>
#include <QDebug>

class Tlacitko : public QPushButton
{
    Q_OBJECT

    QList<QString> barvy;
    int stav;

public:
    Tlacitko (QWidget * parent = 0) : QPushButton (parent), stav(0)
    {
        setStyleSheet("background-color: gray;");
        barvy << "red" << "orange" << "yellow" << "purple" << "magenta";
    }

public slots:
    void zmena ()
    {
        setStyleSheet(QString("background-color: %1;").arg(barvy.at(stav % barvy.size())));
        // diky funkci sender lze ziskat ukazatel na objekt, ktery signal emitoval
        QPushButton *b = dynamic_cast<QPushButton *>(sender());
        b->setText(barvy.at(stav % barvy.size()));
        stav++;
    }

};