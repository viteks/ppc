#include <QApplication>
#include <QPushButton>
#include <QVBoxLayout>
#include <QDebug>
#include <QTimer>

#include "tlacitko.h"

int main(int argc, char ** argv)
{
    QApplication app (argc, argv);

    // okno aplikace
    QWidget w;

    // graficke prvky
    QPushButton b1;
    QPushButton b2;
    Tlacitko b3;

    // propojeni signalu s lambdou
    QObject::connect (&b1, QPushButton::clicked, [&](){
        b1.setText("clicked");
    });

    // propojeni signalu se slotem tridy
    QObject::connect (&b2, QPushButton::clicked, &b3, Tlacitko::zmena);

    QTimer t;
    t.setInterval(1000);
    t.start();

    // propojeni signalu s lambdou
    int cas = 0;
    QObject::connect (&t, QTimer::timeout, [&](){
        b1.setText(QString("%1").arg(5-(cas++)));
        if (cas > 5){
            t.stop();
            b1.setText("bum!");
        }
    });

    // rozlozeni prvku
    QVBoxLayout vbox;
    vbox.addWidget(&b1);
    vbox.addWidget(&b2);
    vbox.addWidget(&b3);

    // okno ziska rozlozeni a vykresli se
    w.setLayout(&vbox);
    w.show();

    return app.exec();
}