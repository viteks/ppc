#include <QApplication>
#include <QMainWindow>

#include "sachovnice.h"

int main(int argc, char ** argv)
{
    QApplication app (argc, argv);

    Sachovnice s(10,10);
    s.show();

    return app.exec();
}