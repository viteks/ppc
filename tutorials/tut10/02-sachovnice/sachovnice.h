#include <QPushButton>
#include <QDebug>
#include <QResizeEvent>

class Sachovnice : public QWidget
{
    QPushButton *b;
    int numX, numY;

public:
    Sachovnice (int x, int y, QWidget * parent = 0) : QWidget (parent), numX(x), numY(y)
    {
        resize(400, 400);

        for (int i = 0; i < numX; i++)
        {
            for (int j = 0; j < numY; j++)
            {
                b = new QPushButton(this);
                b->resize(50, 50);
                b->move(50*i, 50*j);
                connect (b, QPushButton::clicked, this, Sachovnice::info);
            }
        }
    }

    void resizeEvent (QResizeEvent *)
    {
        int W = geometry().width();
        int H = geometry().height();

        QList<QPushButton *> t = findChildren<QPushButton *>();

        for (int i = 0; i < t.size(); i++)
        {
            int x = t.at(i)->x() / t.at(i)->width();
            int y = t.at(i)->y() / t.at(i)->height();

            t.at(i)->resize(int(W/double(numX)), int(H/double(numY)));
            t.at(i)->move(x*int(W/double(numX)), y*int(H/double(numY)));
        }
    }

public slots:
    void info()
    {
        b = dynamic_cast<QPushButton *>(sender());
        b->setStyleSheet("background-color: yellow;");
    }

};