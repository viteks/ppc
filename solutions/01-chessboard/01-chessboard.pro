TEMPLATE = app
TARGET = 01-chessboard
INCLUDEPATH += include
QT += widgets gui
# CONFIG += debug console

HEADERS += include/tile.h include/board.h
SOURCES += src/tile.cpp src/board.cpp src/main.cpp
RESOURCES += resources/chessboard.qrc