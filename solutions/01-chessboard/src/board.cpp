#include "board.h"
#include "tile.h"

Board::Board(QWidget *parent) : QWidget(parent)
{
    maxsize = QGuiApplication::primaryScreen()->geometry().height() - 100;

    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            b = new Tile((i+j)%2 ? "black" : "white", this);
            b->resize (50, 50);
            b->move (50*i, 50*j);
        }
    }
}

void Board::resizeEvent (QResizeEvent *event)
{
    Q_UNUSED(event)
    // zjisteni nove velikosti okna
    // lze vyuzit i event->size()
    int width = this->geometry().width();
    int height = this->geometry().height();
    int size = (width > height) ? width : height;
    // maximalni velikost je dana vyskou displeje - viz konstruktor
    if (size > maxsize) size = maxsize;
    // nova velikost dlazdice
    int tsize = size/8;
    // zmena velikosti okna se zachovanim pomeru stran
    this->resize(8*tsize, 8*tsize);
    // vyhledani vsech potomku kontejneru typu QPushButton
    QList<QPushButton *> buttons = this->findChildren<QPushButton *>();
    // zmena velikosti a pozice vsech dlazdic
    for (int k = 0; k < buttons.size(); k++) 
    {
        int i = buttons.at(k)->x() / buttons.at(k)->width();
        int j = buttons.at(k)->y() / buttons.at(k)->height(); 
        buttons.at(k)->move (i*tsize, j*tsize);
        buttons.at(k)->resize(size/8, size/8);
    }
}