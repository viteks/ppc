#include "tile.h"

Tile::Tile (QString c, QWidget *parent) : QPushButton(parent), color(c), active(false)
{
    updateStyle();
    QObject::connect (this, QPushButton::clicked, this, Tile::changeState);
}

void Tile::changeState ()
{
    active = !active;
    updateStyle();
}

void Tile::updateStyle ()
{
    setProperty("state", QVariant(QString("%1-%2").arg(color).arg(active)));
    // trik pro aktualizaci vlastnosti
    style()->unpolish(this);
    style()->polish(this);
}