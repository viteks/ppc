#include <QPushButton>
#include <QVariant>
#include <QStyle>

class Tile : public QPushButton
{
    Q_OBJECT

    QString color; 
    bool active;

    void updateStyle ();

public:
    Tile (QString, QWidget *parent = 0);

public slots:
    void changeState ();
};