#include <QApplication>
#include <QFile>
#include <QResizeEvent>
#include <QPushButton>
#include <QDebug>

class Board : public QWidget
{
    QPushButton *b;
    int maxsize;

public:
    Board (QWidget *parent = 0);

private:
    void resizeEvent (QResizeEvent *event);
    void drawTiles (int size);
};