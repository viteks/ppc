TEMPLATE = app
TARGET = 03-histogram
INCLUDEPATH += include
# CONFIG += debug console
QT += widgets

HEADERS += include/histogram.h include/dock.h
SOURCES += src/dock.cpp src/histogram.cpp src/main.cpp
