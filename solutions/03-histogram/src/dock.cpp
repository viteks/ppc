#include "dock.h"

Dock::Dock (QWidget *parent) : QWidget (parent)
{
    resize(400, 400);

    histogram = new Histogram;
	
    toolBar = new QToolBar;
    auto openAction = new QAction("Open");
    toolBar->addAction(openAction);
    connect(openAction, SIGNAL(triggered()), this, SLOT(loadFromFile()));

    statusBar = new QStatusBar;
    statusBar->setMaximumHeight(20);
    statusBar->showMessage("No data loaded");

    dockLayout = new QVBoxLayout; 
    dockLayout->setMenuBar(toolBar); 
    dockLayout->addWidget(histogram);
    dockLayout->addWidget(statusBar);
    dockLayout->setContentsMargins(0,0,0,0);
    setLayout(dockLayout);
}

void Dock::loadFromFile()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open Data File"), "",
        tr("Text File (*.txt);;All Files (*)"));
    
    if (fileName.isEmpty())
    {
        return;
    }
    else 
    {
        QFile file(fileName);
        if (file.open(QIODevice::ReadOnly))
        {
            QString data = file.readAll();
            QStringList vals = data.split(' ');
            histogram->clear();
            foreach (QString str, vals) {
                histogram->push_back(str.toInt());
            }
            
            statusBar->showMessage(QString("Loaded data from %1").arg(QFileInfo(fileName).fileName()));
            
            if (histogram->size().width() < 30*vals.size())
            {
                histogram->resize(30*vals.size(), histogram->size().height()*(30*vals.size()/histogram->size().width()));
                setGeometry(x(), y(), 30*vals.size(), height());
            }

            histogram->update();
        }
    }
}