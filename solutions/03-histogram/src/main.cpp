#include <QApplication>

#include "dock.h"


int main (int argc, char ** argv)
{
	QApplication app (argc, argv);

	Dock d;
	d.setWindowTitle("Histogram");
	d.show();

	return app.exec();
}