#include "histogram.h"

Histogram::Histogram (QWidget *parent) : QWidget (parent)
{
    // vytvorim si prazny vektor cisel typu int
    data = new QVector<int>();
}

void Histogram::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event); 

    if (data->size() < 1) return;

    // velikost kreslici plochy pro sloupce histogramu
    width = size().width();
    height = size().height() - 20; // 20 px bude vyska popisky
    // sirka sloupce
    barWidth = (double)width/data->size();

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    painter.fillRect (0, 0, width, height+20, QColor(230, 230, 230));

    for (int i = 0; i < data->size(); i++)
    {
        int d = data->at(i);
        drawBar(&painter, i, d);
        drawLabel(&painter, i, QString::number(d));
    }
}

void Histogram::drawBar (QPainter *p, int position, int value)
{
    QPoint barPosition (barWidth*position, height - (value/100.0)*height);
    QSize  barSize (barWidth, (value/100.0)*height);
    QRectF bar (barPosition, barSize);
        
    QPainterPath path;
    path.addRoundedRect (bar, 2, 2);

    QColor color;
    p->fillPath(path, color.fromHsv(int(position*(350/data->size())), 255, 255));
    QPen pen (color, 1);
    p->setPen(pen);
    p->drawPath(path);
}

void Histogram::drawLabel (QPainter *p, int position, QString value)
{
    QPoint labelPosition (barWidth*position, height);
    QSize labelSize (barWidth, 20); 

    QRect label (labelPosition, labelSize);
    p->fillRect (label, QColor(255, 255, 0, 65));
    p->drawText (label, Qt::AlignCenter | Qt::AlignHCenter, value);
}