#include <QVBoxLayout>
#include <QWidget>
#include <QToolBar>
#include <QStatusBar>
#include <QFileDialog>

#include "histogram.h"

class Dock : public QWidget
{
    Q_OBJECT

    QVBoxLayout *dockLayout;
    Histogram *histogram;
    QToolBar *toolBar;
    QStatusBar *statusBar;
public:
    Dock (QWidget *parent = 0);

private slots:
    void loadFromFile ();
};