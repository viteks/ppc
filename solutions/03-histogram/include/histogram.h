#include <QWidget>
#include <QDebug>
#include <QPainter>
#include <QPainterPath>
#include <iostream>

class Histogram : public QWidget
{
    Q_OBJECT

    QVector<int> *data;
    int width, height;
    double barWidth;

    void drawBar (QPainter *, int, int);
    void drawLabel (QPainter *, int, QString);

public:
    Histogram (QWidget *parent=0);
    void paintEvent(QPaintEvent *event);
    void push_back (int x) {data->push_back(x);}
    void clear() {data->clear();}
};