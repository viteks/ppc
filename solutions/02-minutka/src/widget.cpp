#include "widget.h"

Widget::Widget (QWidget *parent) : QWidget (parent)
{
    // setGeometry(200,200,200,200);
    // setFixedSize(130, 190);
    // setFixedSize(this->geometry().width(),this->geometry().height());
    // setStyleSheet("background-color: #000;");

    stav = 0;
    time = 0;

    button = new QPushButton;
    play = new QIcon(":/resources/images/play.png");
    pause = new QIcon(":/resources/images/pause.png");
    button->setIcon(*play);
    button->setIconSize(QSize(35, 35));

    dial = new CustomDial;
    dial->setFixedSize(120, 120);
    dial->setMaximum(60);
    
    dial->installEventFilter(this);

    lcd = new QLCDNumber;
    lcd->display("00:00");
    lcd->setSegmentStyle(QLCDNumber::Flat);

    hbox.addWidget(lcd);
    hbox.addWidget(button);
    vbox.addLayout(&hbox);
    vbox.addWidget(dial, 0, Qt::AlignHCenter);

    timer = new QTimer(this);

    QObject::connect (timer, QTimer::timeout, [&](){
        dial->setValue(int(time/60));        
        QTime cas (0,0,0);
        time--;
        cas = cas.addSecs(time);
        lcd->display(cas.toString("mm:ss"));
    });
    QObject::connect (dial, QAbstractSlider::valueChanged, [&](){
        if (!stav) 
        {
            time = 60*int(dial->value());
        }
        QTime cas (0,0,0);
        cas = cas.addSecs(time);
        lcd->display(cas.toString("mm:ss"));
    });
    QObject::connect (button, QPushButton::clicked, [&](){
        if (stav == 0)
        {
            stav = 1;
            timer->start(1000);
            button->setIcon(*pause);
            QPalette pal = lcd->palette();
            pal.setColor(pal.WindowText, Qt::red);
            lcd->setPalette(pal);
        }
        else if (stav == 1)
        {
            stav = 0;
            timer->stop();
            button->setIcon(*play);
            QPalette pal = lcd->palette();
            pal.setColor(pal.WindowText, Qt::white);
            lcd->setPalette(pal);
        }
    });

    setLayout(&vbox);
}

bool Widget::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == dial)
    {
        if (event->type() == QEvent::MouseButtonPress)
        { 
            return true;
        }
        return false;
    }
    
    return QWidget::eventFilter(obj, event);
}