#include <QApplication>
#include <QFile>

#include "widget.h"

int main(int argc, char ** argv)
{
	QApplication app(argc, argv);

	Q_INIT_RESOURCE(style);

	QFile file(":/resources/styles/style.qss");
	if(file.open(QFile::ReadOnly))
	{
		QString style = QString::fromLatin1(file.readAll());
		qApp->setStyleSheet(style);
		file.close();
	}

	Widget w;
	w.show();

	return app.exec();
}
