#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLCDNumber>
#include <QTimer>
#include <QPushButton>
#include <QTime>
#include <QEvent>

#include "customdial.h"

class Widget : public QWidget
{
	Q_OBJECT

	int time;
	int stav;

public:
	Widget (QWidget *parent = 0);

	QVBoxLayout vbox;
	QHBoxLayout hbox;
	QLCDNumber *lcd;
	QPushButton *button;
	QDial * dial;
	QTimer *timer;
	QIcon *play, *pause;

	bool eventFilter(QObject *obj, QEvent *event);

public slots:

signals:

};
